# Inital exploration of the Carolina Conversations Collection (CCC)

## Goals:

Classify a subset of CCC containing patient-interviewer dialogues into
two categories: AD and Control.

## Abstract Data Representation:

The basis for our data representation are vocalisation graphs, that is
Markov diagrams encoding the first-order conditional transition
probabilities between vocalisation events (speech, joint talk, pauses,
and switching pauses) and stationary probabilities. Speech rate ratios
also form part of a variant of this basic representation.

## Software:

The software used was:

- R >= 4.0 (https://cran.r-project.org/)
- vocaldia (https://cran.r-project.org/package=vocaldia) >= 0.8.2; for
  handling vocalisation graphs 
- R/ml.R

- the rJava package (for WEKA integration): 
  http://cran.r-project.org/web/packages/rJava/
 
- the RWeka package (a wrapper for the WEKA API):
  http://cran.r-project.org/web/packages/RWeka/
       
- The espeak synthesizer for our speech-rate estimation 'hack'
  (i.e. estimate a 'speaking ratio' as the ratio of actual the
  duration of an utterance to the duration of an utterance synthesized
  by espeak at 160 WPM):
  http://espeak.sourceforge.net/

- (optionally) PA's scrit to extract CSV trasncription profiles from CCC's
  transcription files (.trs)

- (optionally) the graph, igraph and Rgraphviz packages (the latter
  installable with

```r
       source("http://www.bioconductor.org/biocLite.R")
       biocLite("graph")
       biocLite("Rgraphviz")
```

## Analysis / Classification

### Data preparation:

- CSV files were created from [TODO: Sofia/Pierre, add details of data
  selection and csv-generation script]

- Files were concatenated and a new column (dx \in {ad, nonad}) was
  added for the classification target. Words in the transcripts were
  replaced by x's for anomymisation/data licensing compliance. The
  interviewer-patient identifiers (pseudonyms) were kept consistent
  with the CCC identifiers, so if you obtain ethics approval to use
  the original audio and transcripts this dataset can be matched to
  the (anonymised) patient details. The Resulting file is
  data/ccc_dial_subset_df.csv)

- CSV was converted into a data.frame(); silences were detected and
  added to the original turns data through
  cccDialogues.R:loadDataset(), assuming that you start R from
  R/:

```r
  x <- loadDataset()
```

  The dataset has these general speech turn distributions:

```r
   with(subset(x, dx=='nonad'), table(as.character(id), speaker))
                    speaker
                        Floor spk1 spk2 spk3
  Abbott_Maddock_01   0     7   47   22    2
  Abney_Magoon_01     0    16   68   43    2
  Ackart_Main_01      0    23   48   73    0
  Adcock_Magoon_01    0     4    9    0   17
  Addison_Manners_01  0    24   54   72    0
  Adrian_Mans_01      0     4    4   92    0
  Affleck_Mallard_01  0    14   28   67    0
  Agar_Martin_01      0     9   40   58    0
  Agnew_Manser_01     0    13   12   93    0
  Alan_Mann_01        0    10   17   84    0
  Alsop_Mallery_01    0    30   71   71    0
  Annan_Manners_01    0    28   78   83    0
  Appleby_Main--_01   0    12   36   61    0
  Askew_Mather_01     0     7   38   24    0
  Astor_Masten_01     0     5   20   18    0
  Atherton_Masten_01  0    17   71   53    0
  Atkins_May_01       0    12   42   41    0

 with(subset(x, dx=='ad'), table(as.character(id), speaker))
                         speaker
                              Floor spk1 spk2 spk3
  Ainsworth_Main_01         0    23   49   58    0
  Carmel_Nold_001           0    82   21  321  151
  Mason_Davis_001_01        0    38   85  107    0
  Tabor_Aver_001            0    68  158  211    0
  Taggart_Blac_001          0    52  121  115    0
  Tappan_Patte_001          0    50  132  138    0
  Taylor_Morr_001 (1)       0    56  180   83    0
  Tedding_Gree_001_01       0    15   25   66    0
  Teesdale_Creek_001_01     0    26   53   70    0
  Wakefield_Brock_001_01    0    54  161  112    0
  Wallace_Patel_001         0    61  111  177    0
  Waller_Hurle_001-01 (1)   0    19   40   62    0
  Wallop_Wilso_001          0    58  130  102    0
  Walters_Nold_004          0    82   21  321  151
  Walton-Hort               0     1   56   53    0
  Welden_Evans_001          0     0   80   80    1
  Whealdon_Kath_001         0    39   77   65  165
  Wheelock_Evan_001         0    35  137   44    0
  Yager_Hayes_001           0    56  123  153    0
  Yarrow_Berry_001 (2)      0    53  147  129    1
  Zachary_Baile_001         0    34   51  179    0   
```
   
  or by role:

```r
  with(subset(x, dx=='ad'), table(as.character(id), role))
                         role
                          Floor Other patient professional
  Ainsworth_Main_01          23     0      58           49
  Carmel_Nold_001            82   151      21          321
  Mason_Davis_001_01         38     0     107           85
  Tabor_Aver_001             68     0     158          211
  Taggart_Blac_001           52     0     121          115
  Tappan_Patte_001           50     0     132          138
  Taylor_Morr_001 (1)        56     0      83          180
  Tedding_Gree_001_01        15     0      25           66
  Teesdale_Creek_001_01      26     0      53           70
  Wakefield_Brock_001_01     54     0     161          112
  Wallace_Patel_001          61     0     177          111
  Waller_Hurle_001-01 (1)    19     0      62           40
  Wallop_Wilso_001           58     0     130          102
  Walters_Nold_004           82    21     151          321
  Walton-Hort                 1     0     109            0
  Welden_Evans_001            0     1      80           80
  Whealdon_Kath_001          39     0     230           77
  Wheelock_Evan_001          35     0     137           44
  Yager_Hayes_001            56     0     153          123
  Yarrow_Berry_001 (2)       53     1     129          147
  Zachary_Baile_001          34     0     179           51
```


#### Vocalisation-only dataset:

- Each separate dialogue was converted into a 'vocalisation graph'
  (i.e. a Markov diagram and 1st-order transition probabilities and
  stationary probabilities). These diagrams are our basic data
  representation. There are two alternative ways of generating these
  diagrams: a 'sampled' approach
  (```R cccDialogues.R:getSampledVocalMatrix()```) where at regular intervals
  we sample the speech stream and mark the sppech event at that point,
  and a continuous aproach ```R cccDialogues.R:getTurnTakingProbMatrix()```)
  where transitions are taken directly from turn annotations.  To see
  what on such diagram looks like, try for instance:

```r
 getSampledVocalMatrix(subset(x, id=='Taylor_Morr_001 (1)'))
 $ttarray
                        Vocalisation SwitchingPause SwitchingVocalisation
  Vocalisation           0.846153846    0.090659341           0.005494505
  SwitchingPause         0.009174312    0.688073394           0.302752294
  SwitchingVocalisation  0.246376812    0.007246377           0.746376812
  Pause                  0.253012048    0.000000000           0.000000000
                           Pause
  Vocalisation          0.05769231
  SwitchingPause        0.00000000
  SwitchingVocalisation 0.00000000
  Pause                 0.74698795
  attr(,"class")
  [1] "vocalmatrix"

  $tdarray
         Vocalisation        SwitchingPause SwitchingVocalisation 
            0.5284598             0.1550899             0.1976452 
                Pause 
            0.1188051 
  attr(,"class")
  [1] "vocduration"

  attr(,"class")
  [1] "vocaldia"
```
   where ```R ttarray contains``` the graphs adjacency matrix labelled with
   transition probabilities. Note that self-transitions (diagonals)
   dominate the distribution. tdarray gives the prior probabilities
   (e.g. that professional speaks, p(professional)=0.48). 'Floor'
   denotes silence. 

   Alternatively, you can get a matrix without self-transitions:

```r
  getTurnTakingProbMatrix(subset(x, id=='Taylor_Morr_001 (1)'), individual = T)
  $ttarray
                    professional    patient Pause SwitchingPause GrpPause
  professional         0.6722222 0.08888889     0      0.2388889        0
  patient              0.5487805 0.29268293     0      0.1585366        0
  Pause                0.0000000 0.00000000     0      0.0000000        0
  SwitchingPause       0.2321429 0.76785714     0      0.0000000        0
  GrpPause             0.0000000 0.00000000     0      0.0000000        0
  GrpSwitchingPause    0.0000000 0.00000000     0      0.0000000        0
                    GrpSwitchingPause
  professional                      0
  patient                           0
  Pause                             0
  SwitchingPause                    0
  GrpPause                          0
  GrpSwitchingPause                 0
  attr(,"class")
  [1] "vocalmatrix"

  $tdarray
     professional           patient             Pause    SwitchingPause 
        0.4814912         0.2446138         0.0000000         0.2738950 
         GrpPause GrpSwitchingPause 
        0.0000000         0.0000000 
  attr(,"class")
  [1] "vocduration"

  attr(,"class")
  [1] "vocaldia"
```
   
   Note that 'Floor' here has been further categorised a Pauses
   (between two turns of the same speaker), SwitchingPause (a pause
   followed by a different speaker taking of the floor), and group
   pauses (when two or more speakers are speaking at the same time,
   pause, and then start again) which can be GrpPause and
   GrpSwitchingPause, analogously to the individual pauses. 
   
   These graphs can be visualised using igraph or Rgraphviz. E.g.:

```r
  plot(getTurnTakingProbMatrix(subset(x, id=='Abbott_Maddock_01'), individual = T))
  IGRAPH 6e003b7 DNW- 7 13 -- 
  + > 
  attr: layout (g/n), name (v/c), label (v/c), size (v/n), weight
  | (e/n), label (e/n)
  + > 
  edges from 6e003b7 (vertex names):
   [1] professional  ->professional   professional  ->Other         
   [3] professional  ->patient        professional  ->Pause         
   [5] professional  ->SwitchingPause Other         ->Other         
   [7] Other         ->SwitchingPause patient       ->professional  
   [9] patient       ->patient        patient       ->SwitchingPause
  [11] Pause         ->professional   SwitchingPause->professional  
  [13] SwitchingPause->patient       
```

   Finally, we generate a dataset for training/testing our classifiers
   using printARFFfile():

```r
   printARFFfile(x, file="../data/ccc/vocal-only.arff")
```
-  Using this dataset, we train a 'boosting' classifier (a variant of
   Adaboost using class probability estimates; additive logistic
   regression [Friedman et al, 98] with 'decision stumps' as weak
   learners) and test it on the data trough a 10-fold cross-validation
   procedure, e.g.:

```r
     x3 <- runExperiment("../data/ccc/vocal-only.arff", nfold=10, classifier=makeLogitBoostClassifier(), target='dx', onevsallclass='ad')
```

#### Adding speech rate (ratio) information:

- Pierre generated speech rate information from transcription word counts
  and time, and duration of the synthesized transcript (using MaryTTS
  with a normalisation factor to set the normal speech rate at
  160wpm.) The file containing these data is `data/ccc/speechrates.csv`.

  A rate ratio based on speech synthesis can also be estimated by
  cccDialogues.R:getSpeechRateSynthesisRatio2(), which is based on Akira's
  "hack" (Hayakawa et al., 2018):
  
```
             duration of synthesized transcription
  sratio =   -------------------------------------
                  actual duration of  turn 
```

  An sratio variable can be added to the basic dataset by:

```r
sr <- getSpeechRateSynthesisRatio2(x)
x = cbind(x[,1:3],sratio=sr, x[,4:ncol(ds)])
```
    
  This function is used by `ml.R:makeRunExp()` to generate a dataset
  that includes mean(sratio) and var(sratio). We have also used an
  alternative method, namely De Jong’s syllable nuclei detection
  algorithm (Jong and Wempe, 2009), to extract speaking rate directly
  from the audio files. However, as we don't have permission to
  redistribute the audio files and the transcripts had to be
  anonymised in the data included in this distribution, we will use
  the resulting ARFF file `data/vocal.arff`

- ml.R:makeRunExp() was used to run the experiments. For the
  srate-enriched dataset we do, for instance (using leave-one-out
  cross validation):
  
```r
locvexp <- makeRunExp(reusearff=T, nfold=0)
[...]
TRUE                     FALSE
-------------------      ----------------------
Accuracy:   0.905         Accuracy:  0.824
Precision:  0.864         Precision: 0.875 
Recall:     0.905         Recall:    0.824 
F_1:        0.884         F_1:       0.848
Precision:  0.905         Precision: 0.824       (macro-averaged)
Recall:     0.905         Recall:    0.824       (macro-averaged)
F_1:        0.905         F_1:       0.824       (macro-averaged)
Overall accuracy: 0.868

Contingency table:       
        nonad ad
  nonad    14  3
  ad        2 19
[1] "AUC =  0.91"
```


### Results: 

The above procedure gives gives fairly decent accuracy and F-scores,
even without excluding the anomalous dialogues ( Carmel_Nold_001,
Walton-Hort, Welden_Evans_001):

- For the Vocalisation-only dataset (see above), we get (typically):

```r
  printResultsSummary(x3,1)

  Summary for experiment "Prediction Experiment", Avg over 10-fold evaluation for 
  runExperiment("../data/ccc/vocal-only.arff", nfold = 10,
               classifier = makeLogitBoostClassifier(),
               target = "dx", onevsallclass = "ad")
  on classifier classifier and threshold 0.
  =====================================

  TRUE                     FALSE
  -------------------      ----------------------
  Accuracy:   0.812         Accuracy:  0.714
  Precision:  0.765         Precision: 0.769 
  Recall:     0.812         Recall:    0.714 
  F_1:        0.788         F_1:       0.741
  Precision:  0.667         Precision:        (macro-averaged)
  Recall:     0.722         Recall:           (macro-averaged)
  F_1:        0.685         F_1:              (macro-averaged)
  Overall accuracy: 0.767

  Contingency table:       
        nonad ad
  nonad    10  4
  ad        3 13
```


- For the dataset augmented with speech rate data, we get:

```r
  printResultsSummary(x4,1)

  Summary for experiment "Prediction Experiment", Avg over 10-fold evaluation for 
  runExperiment("../data/vocal.arff",
        nfold = 10, classifier = makeLogitBoostClassifier(),
        target = "dx", onevsallclass = "ad")
  on classifier classifier and threshold 0.
  =====================================

  TRUE                     FALSE
  -------------------      ----------------------
  Accuracy:   0.882         Accuracy:  0.769
  Precision:  0.833         Precision: 0.833 
  Recall:     0.882         Recall:    0.769 
  F_1:        0.857         F_1:       0.800
  Precision:  0.796         Precision: 0.708       (macro-averaged)
  Recall:     0.833         Recall:    0.708       (macro-averaged)
  F_1:        0.811         F_1:       0.700       (macro-averaged)
  Overall accuracy: 0.833

  Contingency table:       
        nonad ad
  nonad    10  3
  ad        2 15
```

### Running leave-one-out cross validation (LOOCV).

As the data set is small, we can generate more precise estimates of
performance by using LOOCV (that is, training on the whole dataset
minus one instance, and testing on that instance, and iterating this
procedure for the whole dataset, each time leaving a different
instance out). In order to do this, specify nfold=0 in runExperiment()
or makeRunExp(). E.g.:

```r
x <- makeRunExp(reusearff=T, auc=F, nfold=0)
```

### Comparing different classifiers

We added this at the suggestion of one of the reviewers. For the
different classifiers (Naive Bayes, C4.5 decision trees, SVM (SMO
implementation) and Random Forest), just specify classifier=CLASSIFIER
in runExperiment() or makeRunExp(), where CLASSIFIER is
makeNBClassifier(), makeRandomForestClassifier(), J48 or SMO.


- The ROC curves can be visualised with:

```r
   plotROCCurve(x3)
```

  which in this case gave an AUC of .92 (other runs will typically
  give different values due to the randomisation step in
  `runExperiment()`).

- The ROC curves are very jagged, as there are very few data
  points. We smooth these curves through simulation:

```r
  x6 <- makeRunExp(file="../data/vocal-only.arff", reusearff=T, simulate=10)
```

  which runs 10 rounds of 10-fold cross-validation randomising the
  dataset at each run.

  Then we can save the ROC curves for documentation:

```r
  dev.copy2pdf(file='vocal-only-roc.pdf')
```


## References:

Hayakawa A, Vogel C, Luz S, Campbell N. Speech rate calculations with short utterances: A study from a speech-to-speech, machine translation mediated map task. InProceedings of the Eleventh International Conference on Language Resources and Evaluation (LREC 2018) 2018 May.

Luz S, de la Fuente S, Albert P. A method for analysis of patient speech in dialogue for dementia detection. arXiv preprint arXiv:1811.09919. 2018 Nov 25.
